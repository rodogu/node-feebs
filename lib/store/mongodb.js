var _ = require("underscore");
var db = require('mongodb').Db;
var Q = require("q");

function MongoDBStore(db, collectionName) {
    this._db = db;
    this._collectionName = collectionName;
    _.bindAll(this);
}

var proto = MongoDBStore.prototype;

proto._doWithCollection = function(callback) {
    callback(this._db.collection(this._collectionName));
};

proto.get = function (key) {
    var dfd = Q.defer();
    this.hget(key).then(function (hash) {
            dfd.resolve(hash.value);
        },
        dfd.reject
    );
    return dfd.promise;
};

proto.set = function (key, value) {
    return this.hset(key, {value: value});
};

proto.del = function (key) {
    var dfd = Q.defer();
    this.hdel(key).then(function () {
            dfd.resolve();
        },
        dfd.reject
    );
    return dfd.promise;
};

proto.hget = function (key) {
    var dfd = Q.defer();
    this._doWithCollection(function(collection) {
        collection.findOne({_key : key},function(err,hash) {
            if (! err) {
                dfd.resolve(_.omit(hash,"_id","key"));
            } else {
                throw err;
            }
        });
    });
    return dfd.promise;
};

proto.hset = function (key, hash) {
    var dfd = Q.defer();
    this._doWithCollection(function(collection) {
        collection.update({_key : key}, _.extend(hash,{_key : key}),{safe: true, upsert: true}, function(err) {
            if (! err) {
                dfd.resolve();
            } else {
                throw err;
            }
        });
    });
    return dfd.promise;
};

proto.hdel = function (key) {
    var dfd = Q.defer();
    this._doWithCollection(function(collection) {
        collection.remove({_key : key},true,function(err) {
            if (err) {
                throw err;
            }
            dfd.resolve();
        })
    });
    return dfd.promise;
};

module.exports = function (logger, config) {
    var proxy = {};
    var proxyDfd = Q.defer();
    var store;

    db.connect(config.connection, _.bind(function (err, db) {
        if (! err) {
            logger.info("MongoDB connected");
            proxyDfd.resolve(new MongoDBStore(db,config.collection));
        } else {
            throw err;
        }
    },this));

    proxyDfd.promise.then(function(impl) {
        store = impl;
    });

    ["get", "set", "del", "hget", "hset", "hdel"].forEach(function (name) {
        proxy[name] = function () {
            var args = arguments;
            var dfd = Q.defer();
            proxyDfd.promise.then(function (store) {
                store[name].apply(store, args).then(dfd.resolve, dfd.reject);
            });
            return dfd.promise;
        };
    });

    return proxy;
};
